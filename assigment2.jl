### A Pluto.jl notebook ###
# v0.18.1

using Markdown
using InteractiveUtils

# ╔═╡ c5f56df6-224e-4536-8de8-d5bda936c410
using PlutoUI,DataStructures,Pkg

# ╔═╡ 8f6c1118-0223-46f3-996c-cf7546a6fabd
md"## PROBLEM  1"

# ╔═╡ 88a75f57-c6a7-4aa0-911f-435bdba0fe93
md"## Company X"

# ╔═╡ 5aef0a5a-bcab-42ce-ac48-ce2f6478b63d
md"## Packages"

# ╔═╡ 63fbb12e-e109-47d8-9e01-2bea60968b31
md"""Setting up the Floor"""

# ╔═╡ 614325dd-4e61-4282-a884-4b7b3f6aae67
struct Floor
	name::String
	hasAgent::Bool
	hasDirt::Bool
	#neighbours::Array{Floor}
end

# ╔═╡ 96f0fcda-e2a8-4daa-8ec2-0d8c4c10637c
up = Floor("SecondFloor ", 1 , 1)

# ╔═╡ baeb1ae2-5fa1-437d-b319-54f429f62e61
down = Floor("FirstFloor ", 1, 0)

# ╔═╡ d04d92c3-1ae3-4228-b061-09474ca1e2a8
struct state
	FirstFloor::Floor
	SecondFloor::Floor
end

# ╔═╡ baf9b47b-14c8-4827-be72-615087d33542
md"""Movement Functions"""

# ╔═╡ b85fbcec-a13c-4b05-a56f-854ef9fab521
struct action
	Name::String
	Cost::Int64
end


# ╔═╡ c619e56f-e89a-4f7d-8b48-f79ad4effc5c
ME = action("Move East",1)

# ╔═╡ a5c0301f-45e6-4469-83ec-0ca7bbb3b2d5
MW = action("Move West",1)

# ╔═╡ 90ec586b-9a84-4ba0-8eea-8b352d4e3e9d
MU = action("Move Up",1)

# ╔═╡ c6a04d45-ddba-4fa7-9502-a18e3fea26ed
MD = action("Move Down",1)


# ╔═╡ 00504df7-f893-44df-9664-2a70497e4f30
CO = action("Collect Object",2)

# ╔═╡ e77c6387-6ce6-4449-b061-31658fc42416
md"""Function Model"""

# ╔═╡ 47001947-ab7e-4340-abba-f104326a1815
function transitionModel (state,action)
      transtions::Dic{action,state}= Dic()
	 if state.FirstFloor.hasAgent
		 if state.FirstFloor.hasDirt
			transtions[CO] = state(Floor("1st", 0, 1),state.SecondFloor)
		 end
		 
		 transtions[ME] = state(Floor("1st", state.FirstFloor.hasDirt ? 1, 0, 0),Floor("1stFlr", state.SecondFloor.hasDirt ? 1, 0,1),state.SecondFloor)
	 
	 else if state.SecondFloor.hasAgent
		  if state.SecondFloor.hasDirt
			  transtions[CO] = state(Floor("2nd", 0, 1),state.SecondFloor)
		  end
		 
		 transtions[MU] = state(Floor("1st", state.FirstFloor.hasDirt ? 1, 0, 0),Floor ("2ndFlr", state.SecondFloor.hasDirt ? 1, 0,1))
	 
	 else if state.SecondFloor.hasAgent
		  if state.SecondFloor.hasDirt
			  transtions[CO] = state(Floor("1st", 0, 1),state.SecondFloor)
		  end
		 
		 transtions[MU] = state(Floor("2nd", state.FirstFloor.hasDirt ? 1, 0, 0),Floor("2ndFlr", state.SecondFloor.hasDirt ? 1, 0,1))
		 
		 transtions[MD] = state(Floor("1st", state.FirstFloor.hasDirt ? 1, 0, 1),Floor("1stFlr", state.SecondFloor.hasDirt ? 1, 0,1),state.SecondFloor.hasDirt)
	 
	 else if state.SecondFloor.hasAgent
		  if state.SecondFloor.hasDirt
			  transtions[CO] = state(Floor("2nd", 0, 1),state.SecondFloor)
		  end
		 
		 transtions[ME] = state(Floor("1st", state.FirstFloor.hasDirt ? 1, 0, 0),Floor ("2ndFlr", state.SecondFloor.hasDirt ? 1, 0,1))
		 
		 transtions[MD] = state(Floor("2nd", state.FirstFloor.hasDirt ? 1, 0, 1),Floor("1stFlr", state.SecondFloor.hasDirt ? 1, 0,1),state.SecondFloor.hasDirt)
		  
	 else if state.SecondFloor.hasAgent
		  if state.SecondFloor.hasDirt
			  transtions[MU] = state(Floor("2nd", 0, 1),state.SecondFloor)
		  end
		 
		 transtions[ME] = state(Floor("1st", state.FirstFloor.hasDirt ? 1, 0, 0),
			 Floor ,("2ndFlr", state.SecondFloor.hasDirt ? 1, 0,1))
		 
		transtions[MU] = state(Floor("2nd", state.FirstFloor.hasDirt ? 1, 0, 1),Floor("1stFlr", state.SecondFloor.hasDirt ? 1, 0,1),state.SecondFloor.hasDirt)
	
	 else if state.SecondFloor.hasAgent
		 if state.SecondFloor.hasDirt
			 transtions[CO] = state(Floor("1st", 0, 1),state.SecondFloor)
		 end
		 
		 transtions[MW] = state(Floor("2nd", state.FirstFloor.hasDirt ? 1, 0, 1),Floor("1stFlr", state.SecondFloor.hasDirt ? 1, 0,0))
	end
		 
		 return transitions
	 end

# ╔═╡ cd6e9586-5c26-4c52-9691-c3faa5ad0dc8
transitionModel(StartingPosition)

# ╔═╡ 1a2a314b-a544-4122-a3f8-d0379ac36e06
DFS(StartingPosition)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
DataStructures = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
Pkg = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"
PlutoUI = "7f904dfe-b85e-4ff6-b463-dae2292396a8"

[compat]
DataStructures = "~0.18.11"
PlutoUI = "~0.7.38"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.2"
manifest_format = "2.0"

[[deps.AbstractPlutoDingetjes]]
deps = ["Pkg"]
git-tree-sha1 = "8eaf9f1b4921132a4cff3f36a1d9ba923b14a481"
uuid = "6e696c72-6542-2067-7265-42206c756150"
version = "1.1.4"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.ColorTypes]]
deps = ["FixedPointNumbers", "Random"]
git-tree-sha1 = "024fe24d83e4a5bf5fc80501a314ce0d1aa35597"
uuid = "3da002f7-5984-5a60-b8a6-cbb66c0b333f"
version = "0.11.0"

[[deps.Compat]]
deps = ["Base64", "Dates", "DelimitedFiles", "Distributed", "InteractiveUtils", "LibGit2", "Libdl", "LinearAlgebra", "Markdown", "Mmap", "Pkg", "Printf", "REPL", "Random", "SHA", "Serialization", "SharedArrays", "Sockets", "SparseArrays", "Statistics", "Test", "UUIDs", "Unicode"]
git-tree-sha1 = "b153278a25dd42c65abbf4e62344f9d22e59191b"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "3.43.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "3daef5523dd2e769dad2365274f760ff5f282c7d"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.11"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.DelimitedFiles]]
deps = ["Mmap"]
uuid = "8bb1440f-4735-579b-a4ab-409b98df4dab"

[[deps.Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[deps.Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.FixedPointNumbers]]
deps = ["Statistics"]
git-tree-sha1 = "335bfdceacc84c5cdf16aadc768aa5ddfc5383cc"
uuid = "53c48c17-4a7d-5ca2-90c5-79b7896eea93"
version = "0.8.4"

[[deps.Hyperscript]]
deps = ["Test"]
git-tree-sha1 = "8d511d5b81240fc8e6802386302675bdf47737b9"
uuid = "47d2ed2b-36de-50cf-bf87-49c2cf4b8b91"
version = "0.0.4"

[[deps.HypertextLiteral]]
git-tree-sha1 = "2b078b5a615c6c0396c77810d92ee8c6f470d238"
uuid = "ac1192a8-f4b3-4bfe-ba22-af5b92cd3ab2"
version = "0.9.3"

[[deps.IOCapture]]
deps = ["Logging", "Random"]
git-tree-sha1 = "f7be53659ab06ddc986428d3a9dcc95f6fa6705a"
uuid = "b5f81e59-6552-4d32-b1f0-c071b021bf89"
version = "0.2.2"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "3c837543ddb02250ef42f4738347454f95079d4e"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.3"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[deps.Parsers]]
deps = ["Dates"]
git-tree-sha1 = "3b429f37de37f1fc603cc1de4a799dc7fbe4c0b6"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.3.0"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.PlutoUI]]
deps = ["AbstractPlutoDingetjes", "Base64", "ColorTypes", "Dates", "Hyperscript", "HypertextLiteral", "IOCapture", "InteractiveUtils", "JSON", "Logging", "Markdown", "Random", "Reexport", "UUIDs"]
git-tree-sha1 = "670e559e5c8e191ded66fa9ea89c97f10376bb4c"
uuid = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
version = "0.7.38"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.SharedArrays]]
deps = ["Distributed", "Mmap", "Random", "Serialization"]
uuid = "1a1011a3-84de-559e-8e89-a11a2f7dc383"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╠═8f6c1118-0223-46f3-996c-cf7546a6fabd
# ╠═88a75f57-c6a7-4aa0-911f-435bdba0fe93
# ╠═5aef0a5a-bcab-42ce-ac48-ce2f6478b63d
# ╠═c5f56df6-224e-4536-8de8-d5bda936c410
# ╠═63fbb12e-e109-47d8-9e01-2bea60968b31
# ╠═614325dd-4e61-4282-a884-4b7b3f6aae67
# ╠═96f0fcda-e2a8-4daa-8ec2-0d8c4c10637c
# ╠═baeb1ae2-5fa1-437d-b319-54f429f62e61
# ╠═d04d92c3-1ae3-4228-b061-09474ca1e2a8
# ╠═baf9b47b-14c8-4827-be72-615087d33542
# ╠═b85fbcec-a13c-4b05-a56f-854ef9fab521
# ╠═c619e56f-e89a-4f7d-8b48-f79ad4effc5c
# ╠═a5c0301f-45e6-4469-83ec-0ca7bbb3b2d5
# ╠═90ec586b-9a84-4ba0-8eea-8b352d4e3e9d
# ╠═c6a04d45-ddba-4fa7-9502-a18e3fea26ed
# ╠═00504df7-f893-44df-9664-2a70497e4f30
# ╠═e77c6387-6ce6-4449-b061-31658fc42416
# ╠═47001947-ab7e-4340-abba-f104326a1815
# ╠═cd6e9586-5c26-4c52-9691-c3faa5ad0dc8
# ╠═1a2a314b-a544-4122-a3f8-d0379ac36e06
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
